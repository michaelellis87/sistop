package com.teamandroid.mike.aybpeterson.aliciaybernardosol1

import com.f2prateek.dart.HensonNavigable
import com.teamandroid.mike.aybpeterson.BaseActivity

/**
 * Created by mike on 07/06/2018
 */
@HensonNavigable
open class AYBSol1Activity : BaseActivity() {
  override fun createFragment() = AliciaYBernardoSol1Fragment()
}