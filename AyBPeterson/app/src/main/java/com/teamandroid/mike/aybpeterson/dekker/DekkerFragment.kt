package com.teamandroid.mike.aybpeterson.dekker

import android.os.Bundle
import android.view.View
import com.teamandroid.mike.aybpeterson.BaseDemonstrationFragment
import com.teamandroid.mike.aybpeterson.R
import kotlinx.android.synthetic.main.fragment_tests.*

/**
 * Created by mike on 07/06/2018
 */
class DekkerFragment : BaseDemonstrationFragment() {
  var procesoFavorecido: Actor = Actor.ALICE
  var aliceDeseaEntrar: Boolean = false
  var bernardDeseaEntrar: Boolean = false

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    cartelTextView.visibility = View.VISIBLE
    cartelTextView.text = getString(R.string.proceso_favorecido, "")
    aliciaTimes.text = getString(R.string.desea_entrar, Actor.ALICE,
        aliceDeseaEntrar.toString())
    bernardoTimes.text = getString(R.string.desea_entrar, Actor.BERNARD,
        bernardDeseaEntrar.toString())
    descriptionText.visibility = View.INVISIBLE
  }

  override fun startForActor(actor: Actor) {
    Thread(Runnable {
      while (processIsGoing && isViewAlive && isDogAlive(actor)) {
        if (!isGoingForAWalk(actor)) {
          setDeseaEntraFor(actor)
          val otherActor = getOtherActor(actor)
          while (deseaEntrarActor(otherActor)) {
            if (procesoFavorecido == otherActor) {
              setDeseaEntraFor(actor, false)
              while (procesoFavorecido == otherActor) {
              }
              setDeseaEntraFor(actor)
            }
          }
          setIsTakingAWalk(actor, true)
          activity?.runOnUiThread {
            if (processIsGoing && isDogAlive(actor)) {
              startTakingAWalk(actor, Runnable {
                changeProcesoFavorecido(otherActor)
                setDeseaEntraFor(actor, false)
                setIsTakingAWalk(actor, false)
              })
            }
          }
        }
      }
    }).start()
  }

  private fun changeProcesoFavorecido(actor: Actor) {
    procesoFavorecido = actor
    activity?.runOnUiThread {
      cartelTextView.text = getString(R.string.proceso_favorecido, actor)
    }
  }

  private fun setDeseaEntraFor(actor: Actor, deseaEntrar: Boolean = true) {
    if (actor == Actor.ALICE) {
      aliceDeseaEntrar = deseaEntrar
      activity?.runOnUiThread {
        aliciaTimes.text = getString(R.string.desea_entrar, actor, aliceDeseaEntrar.toString())
      }
    } else {
      bernardDeseaEntrar = deseaEntrar
      activity?.runOnUiThread {
        bernardoTimes.text = getString(R.string.desea_entrar, actor, bernardDeseaEntrar.toString())
      }
    }
  }

  private fun deseaEntrarActor(actor: Actor): Boolean =
      if (actor == Actor.ALICE) aliceDeseaEntrar else bernardDeseaEntrar

  override fun startProcess() {
    resetAll()
    showKillDogsOption(true)
    processIsGoing = true
    procesoFavorecido = Actor.ALICE
    aliceDeseaEntrar = false
    bernardDeseaEntrar = false
    // Processes will be concurrent because they will be triggered in separrate Threads
    startForActor(Actor.ALICE)
    startForActor(Actor.BERNARD)
  }
}