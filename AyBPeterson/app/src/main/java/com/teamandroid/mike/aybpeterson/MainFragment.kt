package com.teamandroid.mike.aybpeterson

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_home.*

/**
 * Created by mike on 03/06/2018
 */
class MainFragment : Fragment() {
  @get:LayoutRes
  private val layoutResId = R.layout.fragment_home

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
      inflater.inflate(layoutResId, container, false)

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    aliciaYBernardoSol1Button.setOnClickListener {
      val aYBIntent = Henson.with(context)
          .gotoAYBSol1Activity()
          .build()
      startActivity(aYBIntent)
    }

    aliciaYBernardoWithSemaphoresButton.setOnClickListener {
      val aYBWithSemaphoreIntent = Henson.with(context)
          .gotoAYBWithSemaphoresActivity()
          .build()
      startActivity(aYBWithSemaphoreIntent)
    }

    homePetersonButton.setOnClickListener {
      val petersonIntent = Henson.with(context)
          .gotoPetersonActivity()
          .build()
      startActivity(petersonIntent)
    }

    dekkerButton.setOnClickListener {
      val dekkerIntent = Henson.with(context)
          .gotoDekkerActivity()
          .build()
      startActivity(dekkerIntent)
    }
  }
}