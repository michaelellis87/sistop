package com.teamandroid.mike.aybpeterson

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ValueAnimator
import android.os.Bundle
import android.os.Handler
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import kotlinx.android.synthetic.main.fragment_tests.*

/**
 * Created by mike on 03/06/2018
 */
abstract class BaseDemonstrationFragment : Fragment() {
  protected val isViewAlive: Boolean
    get() = isAdded && activity != null

  companion object {
    const val ANIMATION_LENGTH: Long = 1000
    const val TAKING_A_WALK_DURATION: Long = 2000
  }

  enum class Actor {
    ALICE,
    BERNARD
  }

  protected var processIsGoing = false
  @get:LayoutRes
  private val layoutResId = R.layout.fragment_tests

  private var bernardoX: Float = 0f
  private var aliciaX: Float = 0f
  private var bernardoXAnim = 0f
  private var bernardoYAnim = 0f
  private var aliciaXAnim = 0f
  private var aliciaYAnim = 0f
  protected var aliceIsTakingAWalk = false
  protected var bernardIsTakingAWalk = false
  protected var bernardDogIsAlive = true
  protected var aliceDogIsAlive = true
  protected var aliceCouldntGo = 0
  protected var bernardCouldntGo = 0

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
      inflater.inflate(layoutResId, container, false)

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    resetAll()

    view.postDelayed({
      //Setup for animations
      bernardoX = bernardo.x
      aliciaX = alicia.x
      bernardoXAnim = (view.width / 2).toFloat() - bernardoX - bernardo.width / 2
      bernardoYAnim = (view.height / 2).toFloat() - bernardo.y
      aliciaXAnim = -(view.width / 2).toFloat() + alicia.width / 2
      aliciaYAnim = -(view.height / 2).toFloat() + alicia.height / 2
    }, 100)

    start.setOnClickListener { onStartClicked() }

    killAliceDog.setOnClickListener { onKillDogClicked(Actor.ALICE) }

    killBernardsDog.setOnClickListener { onKillDogClicked(Actor.BERNARD) }
  }

  private fun onStartClicked() {
    if (processIsGoing) {
      stopProcess()
      start.text = getString(R.string.start)
    } else {
      startProcess()
      start.text = getString(R.string.stop)
    }
  }

  private fun stopProcess() {
    processIsGoing = false
    showKillDogsOption(false)
  }

  protected fun resetAll() {
    activity?.runOnUiThread({
      bernardIsTakingAWalk = false
      aliceIsTakingAWalk = false
      setDogAlive(Actor.ALICE, true)
      setDogAlive(Actor.BERNARD, true)
      aliciaTimes.text = getString(R.string.alicia_times, 0)
      bernardoTimes.text = getString(R.string.bernardo_times, 0)
    })
  }

  protected fun isGoingForAWalk(actor: Actor) =
      if (actor == Actor.ALICE) aliceIsTakingAWalk
      else bernardIsTakingAWalk

  protected fun setIsTakingAWalk(actor: Actor, isTakingAWalk: Boolean) =
      if (actor == Actor.ALICE) aliceIsTakingAWalk = isTakingAWalk
      else bernardIsTakingAWalk = isTakingAWalk

  protected fun showKillDogsOption(show: Boolean) {
    killAliceDog.visibility = if (show) View.VISIBLE else View.GONE
    killBernardsDog.visibility = if (show) View.VISIBLE else View.GONE
  }

  private fun onKillDogClicked(actor: Actor) {
    if (isDogAlive(actor)) {
      setDogAlive(actor, false)
    } else {
      setDogAlive(actor, true)
    }
  }

  private fun setDogAlive(actor: Actor, isAlive: Boolean) {
    if (actor == Actor.ALICE) {
      aliceDogIsAlive = isAlive
      if (isAlive) {
        context?.let {
          alicia.setImageDrawable(ContextCompat.getDrawable(it, R.drawable.icon_alicia))
        }
        killAliceDog.text = getString(R.string.kill_alice_dog)
        restartOngoingProcessFor(actor)
      } else {
        context?.let {
          alicia.setImageDrawable(ContextCompat.getDrawable(it, R.drawable.rip_dog))
        }
        killAliceDog.text = getString(R.string.re_encarnate_alice_dog)
      }
    } else {
      bernardDogIsAlive = isAlive
      if (isAlive) {
        context?.let {
          bernardo.setImageDrawable(ContextCompat.getDrawable(it, R.drawable.icon_bernardo))
        }
        restartOngoingProcessFor(actor)
        killBernardsDog.text = getString(R.string.kill_bernards_dog)
      } else {
        context?.let {
          bernardo.setImageDrawable(ContextCompat.getDrawable(it, R.drawable.rip_dog))
        }
        killBernardsDog.text = getString(R.string.re_encarnate_bernards_dog)
      }
    }
  }

  protected fun restartOngoingProcessFor(actor: Actor) {
    if (processIsGoing) startForActor(actor)
  }

  protected fun isDogAlive(actor: Actor) = when (actor) {
    Actor.ALICE -> aliceDogIsAlive
    Actor.BERNARD -> bernardDogIsAlive
  }

  protected fun getOtherActor(actor: Actor) = when (actor) {
    Actor.BERNARD -> Actor.ALICE
    Actor.ALICE -> Actor.BERNARD
  }

  protected fun startTakingAWalk(actor: Actor, doWhenFinished: Runnable? = null) {
    val onFinishTakingAWalk = Runnable {
      Handler()
          .postDelayed(endTakingAWalk(actor, doWhenFinished), TAKING_A_WALK_DURATION)
    }
    if (actor == Actor.BERNARD) {
      bernardIsTakingAWalk = true
      activity?.runOnUiThread({
        anim(bernardo, bernardoXAnim, bernardoYAnim, false,
            onFinishTakingAWalk)
      })
    } else {
      aliceIsTakingAWalk = true
      activity?.runOnUiThread({
        anim(alicia, aliciaXAnim, aliciaYAnim, false,
            onFinishTakingAWalk)
      })
    }
  }

  private fun endTakingAWalk(actor: Actor, doWhenFinished: Runnable? = null): Runnable {
    return Runnable {
      if (actor == Actor.BERNARD) {
        activity?.runOnUiThread({
          anim(bernardo, bernardoXAnim, bernardoYAnim, true,
              doWhenFinished)
        })
      } else {
        activity?.runOnUiThread({
          anim(alicia, aliciaXAnim, aliciaYAnim, true,
              doWhenFinished)
        })
      }
    }
  }

  fun anim(viewToAnimate: View, xAnim: Float, yAnim: Float, reverse: Boolean,
           doWhenFinished: Runnable? = null) {
    val animator1 = ValueAnimator.ofFloat(0f, xAnim)
    val animator2 = ValueAnimator.ofFloat(0f, yAnim)

    animator1.addUpdateListener {
      val value = it.animatedValue as Float
      viewToAnimate.translationX = value
    }

    animator2.addUpdateListener {
      val value = it.animatedValue as Float
      viewToAnimate.translationY = value
    }

    val animatorSet = AnimatorSet()
    animatorSet.playTogether(animator1, animator2)
    animatorSet.duration = ANIMATION_LENGTH
    if (reverse) animatorSet.interpolator = ReverseInterpolator(AccelerateInterpolator())

    animatorSet.addListener(object : AnimatorListenerAdapter() {
      override fun onAnimationEnd(animation: Animator?) {
        super.onAnimationEnd(animation)
        doWhenFinished?.run()
      }
    })
    animatorSet.start()
  }

  abstract fun startProcess()
  abstract fun startForActor(actor: Actor)
}