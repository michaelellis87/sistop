package com.teamandroid.mike.aybpeterson

import com.f2prateek.dart.HensonNavigable

@HensonNavigable
open class MainActivity : BaseActivity() {
  override fun createFragment() = MainFragment()
}
