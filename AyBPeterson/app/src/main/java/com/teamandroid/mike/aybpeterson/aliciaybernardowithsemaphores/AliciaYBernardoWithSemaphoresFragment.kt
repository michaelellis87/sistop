package com.teamandroid.mike.aybpeterson.aliciaybernardowithsemaphores

import android.os.Bundle
import android.view.View
import com.teamandroid.mike.aybpeterson.BaseDemonstrationFragment
import com.teamandroid.mike.aybpeterson.R
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_tests.*
import java.util.concurrent.Semaphore
import java.util.concurrent.TimeUnit

/**
 * Created by mike on 06/06/2018
 */
class AliciaYBernardoWithSemaphoresFragment : BaseDemonstrationFragment() {
  companion object {
    const val DOGS_ALLOWED_AT_SAME_TIME = 1
  }

  //Semaphore that tells the owners when the garden is available
  private var gardenSemaphore: Semaphore = Semaphore(DOGS_ALLOWED_AT_SAME_TIME)

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    redLight.visibility = View.VISIBLE
    greenLight.visibility = View.VISIBLE
    aliciaTimes.visibility = View.GONE
    bernardoTimes.visibility = View.GONE
    descriptionText.text = ""
    setSemaphoreAvailable(true)
  }

  private fun setSemaphoreAvailable(available: Boolean) {
    activity?.runOnUiThread {
      redLight.setImageResource(if (available) R.drawable.circle_grey else R.drawable.circle_red)
      greenLight.setImageResource(if (available) R.drawable.circle_green else R.drawable.circle_grey)
      if (available) descriptionText.text = ""
    }
  }

  override fun startProcess() {
    resetAll()
    processIsGoing = true
    showKillDogsOption(true)
    // Processes will be concurrent because they will be triggered by async observables
    startForActor(Actor.ALICE)
    startForActor(Actor.BERNARD)
  }

  override fun startForActor(actor: Actor) {
    //For showing purposes, alice dog will want to go more often to the garden, and will be able
    //to go when the semaphore allows
    val intervalLong = (if (actor == Actor.ALICE) (1..5) else (10..15)).shuffled().first().toLong()

    Flowable
        .interval(intervalLong, TimeUnit.SECONDS)
        .observeOn(Schedulers.io())
        .subscribeOn(AndroidSchedulers.mainThread())
        //We will filter emissions only when the dog is not taking a walk already
        .filter({ !isGoingForAWalk(actor) })
        //Lives while the semaphore solution is running,
        //the dog is alive and the view is present (This is an android ui check)
        .takeWhile({ processIsGoing && isDogAlive(actor) && isViewAlive })
        .subscribe({
          activity?.runOnUiThread {
            descriptionText.text = getString(R.string.wants_to_enter_but_waits, actor.toString())
          }
          gardenSemaphore.acquire()
          activity?.runOnUiThread {
            descriptionText.text = ""
          }
          if (isDogAlive(actor)) {
            setSemaphoreAvailable(false)
            setIsTakingAWalk(actor, true)
            startTakingAWalk(actor, Runnable {
              setSemaphoreAvailable(true)
              gardenSemaphore.release()
              setIsTakingAWalk(actor, false)
            })
          } else {
            gardenSemaphore.release()
          }
        })
  }
}