package com.teamandroid.mike.aybpeterson.aliciaybernardowithsemaphores

import com.f2prateek.dart.HensonNavigable
import com.teamandroid.mike.aybpeterson.BaseActivity

/**
 * Created by mike on 07/06/2018
 */
@HensonNavigable
open class AYBWithSemaphoresActivity : BaseActivity() {
  override fun createFragment() = AliciaYBernardoWithSemaphoresFragment()
}