package com.teamandroid.mike.aybpeterson

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import com.f2prateek.dart.Dart
import com.f2prateek.dart.HensonNavigable

@HensonNavigable
abstract class BaseActivity : AppCompatActivity() {
  abstract fun createFragment(): Fragment

  private val layoutResId: Int
    @LayoutRes
    get() = R.layout.activity_fragment

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    Dart.inject(this)
    setContentView(layoutResId)

    var fragment = singleFragment
    if (fragment == null) {
      fragment = createFragment()
      supportFragmentManager
          .beginTransaction()
          .add(R.id.fragment_container, fragment)
          .commit()
    }
  }

  private val singleFragment: Fragment?
    get() {
      val fragmentManager = supportFragmentManager
      return fragmentManager.findFragmentById(R.id.fragment_container)
    }
}
