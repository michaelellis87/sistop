package com.teamandroid.mike.aybpeterson.dekker

import com.f2prateek.dart.HensonNavigable
import com.teamandroid.mike.aybpeterson.BaseActivity

/**
 * Created by mike on 07/06/2018
 */
@HensonNavigable
open class DekkerActivity : BaseActivity() {
  override fun createFragment() = DekkerFragment()
}