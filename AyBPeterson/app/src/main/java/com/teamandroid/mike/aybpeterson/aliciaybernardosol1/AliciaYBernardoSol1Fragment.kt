package com.teamandroid.mike.aybpeterson.aliciaybernardosol1

import android.os.Bundle
import android.view.View
import com.teamandroid.mike.aybpeterson.BaseDemonstrationFragment
import com.teamandroid.mike.aybpeterson.R
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_tests.*
import java.util.concurrent.TimeUnit

/**
 * Created by mike on 03/06/2018
 */
class AliciaYBernardoSol1Fragment : BaseDemonstrationFragment() {
  //Cartel is the board that tells whose turn is
  //When a dog ends his walk, its owner will change it to the other dog, so the other one
  //can take a walk
  private var cartel: Actor? = null

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    cartelTextView.visibility = View.VISIBLE
    cartelTextView.text = ""
  }

  override fun startProcess() {
    resetAll()
    showKillDogsOption(true)
    processIsGoing = true
    cartel = null
    // Processes will be concurrent because they will be triggered by async observables
    startForActor(Actor.ALICE)
    startForActor(Actor.BERNARD)
  }

  override fun startForActor(actor: Actor) {
    Flowable
        //Emissions will be shot in a random interval of 1-5 seconds
        .interval((1..5).shuffled().first().toLong(), TimeUnit.SECONDS)
        .observeOn(Schedulers.io())
        .subscribeOn(AndroidSchedulers.mainThread())
        //This will filter only emissions when the dog is not being walked
        .filter({ !isGoingForAWalk(actor) })
        //This will terminate emissions when process has been stopped, dog is dead
        //or the view is not present (This is an Android check)
        .takeWhile({ processIsGoing && isDogAlive(actor) && isViewAlive })
        .subscribe({
          //First one that wants to take a walk puts the name on the board to initialize board
          if (cartel == null) {
            changeBoard(actor)
          }

          if (cartel == actor) {
            startTakingAWalk(actor, Runnable {
              val otroActor = getOtherActor(actor)
              changeBoard(otroActor)
              setIsTakingAWalk(actor, false)
            })
          } else {
            if (actor == Actor.ALICE) aliceCouldntGo++ else bernardCouldntGo++
            activity?.runOnUiThread({
              aliciaTimes.text = getString(R.string.alicia_times, aliceCouldntGo)
              bernardoTimes.text = getString(R.string.bernardo_times, bernardCouldntGo)
            })
          }
        })
  }

  private fun changeBoard(actor: Actor) {
    cartel = actor
    activity?.runOnUiThread {
      cartelTextView.text = getString(R.string.cartel, actor)
    }
  }
}