package com.teamandroid.mike.aybpeterson.peterson

import com.f2prateek.dart.HensonNavigable
import com.teamandroid.mike.aybpeterson.BaseActivity

/**
 * Created by mike on 07/06/2018
 */
@HensonNavigable
open class PetersonActivity : BaseActivity() {
  override fun createFragment() = PetersonFragment()
}