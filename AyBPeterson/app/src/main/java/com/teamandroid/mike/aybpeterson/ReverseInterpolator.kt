package com.teamandroid.mike.aybpeterson

import android.view.animation.Interpolator

/**
 * Created by mike on 05/06/2018
 */
class ReverseInterpolator(private val mInterpolator: Interpolator) : Interpolator {
  override fun getInterpolation(input: Float): Float {
    return 1 - mInterpolator.getInterpolation(input)
  }
}